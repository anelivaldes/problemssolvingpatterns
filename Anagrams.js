/*
    Given two strings, write a function to determine if the second string is an anagram of the first.
    An anagram is a word, phrase, or name formed by rearranging the letters of another,
    such as cinema, formed from iceman.

    validAnagram('', '') // true
    validAnagram('aaz', 'zza') // false
    validAnagram('anagram', 'nagaram') // true
    validAnagram("rat","car") // false
    validAnagram('awesome', 'awesom') // false
    validAnagram('qwerty', 'qeywrt') // true
    validAnagram('texttwisttime', 'timetwisttext') // true

 */

function validAnagram(str1, str2) {
    if (str1.length !== str2.length) {
        return false
    }
    if (str1.length === 0) {
        return true
    }
    let frequency = {}

    for (let i = 0; i < str1.length; i++) {
        let key = str1.charAt(i);
        frequency[key] = (frequency[key] || 0) + 1
    }

    for (let i = 0; i < str2.length; i++) {
        let key = str2.charAt(i);
        if (!(key in frequency)) {
            return false;
        } else {
            frequency[key] -= 1
        }
    }
    for (let key in frequency) {
        if (frequency[key] !== 0) {
            return false;
        }
    }
    return true;
}

let data = [
    ['aaz', 'zza'],
    ['anagram', 'nagaram'],
    ["rat", "car"],
    ['awesome', 'awesom'],
    ['qwerty', 'qeywrt'],
    ['texttwisttime', 'timetwisttext'],
    ['corazon', 'nozaroc'],
    ['retyue', 'retyui'],
]

function validAnagramV1(str1, str2) {
    const freq = {};
    if (str1.length !== str2.length) {
        return false;
    }
    for (let l of str1) {
        if (!freq[l]) {
            freq[l] = 1
        } else {
            freq[l]++;
        }
    }
    for (let l of str2) {
        if (!freq[l]) {
            return false
        } else {
            freq[l]--;
        }
    }
    for (let k in freq) {
        if (freq[k] !== 0) {
            return false
        }
    }
    return true;
}

exports.func = validAnagramV1;
exports.data = data;

