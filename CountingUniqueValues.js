/*

Implement a function called countUniqueValues, which accepts a sorted array,
and counts the unique values in the array.
There can be negative numbers in the array,
but it will always be sorted.

countUniqueValues([1,1,1,1,1,2]) // 2
countUniqueValues([1,2,3,4,4,4,7,7,12,12,13]) // 7
countUniqueValues([]) // 0
countUniqueValues([-2,-1,-1,0,1]) // 4

*/

/* Solution using object and works for any array even if it not sorted */

function countUniqueValues(array) {
    let unique = {};
    for (let v of array) {
        if (unique[v] === undefined) {
            unique[v] = v;
        }
    }
    return Object.keys(unique).length
}

/*
   Solution without object and with a sorted array, using to pointers
   one af 0 and one at 1, the idea is than when fine a different value
   will copy on the i position and at the end the unique values will
   be at the beginning of the array and i will be the lenght of that
   sub-array, so need to plus 1
*/

function countUniqueValues_refactor(array) {
    if (array.length === 0) return 0;
    let i = 0;
    for (let j = 1; j < array.length; j++) {
        if (array[i] !== array[j]) {
            i++;
            array[i] = array[j]
        }
    }
    return i + 1
}

let data = [
    [[1, 1, 1, 1, 1, 2]],
    [[1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]],
    [[]],
    [[-2, -1, -1, 0, 1]],
    [[1, 2, 2, 5, 7, 7, 99]]

]

exports.func = countUniqueValues;
exports.data = data;
