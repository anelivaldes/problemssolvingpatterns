/*

Given a sorted array of integers, write a function called search,
that accepts a value and returns the index where the value passed
to the function is located. If the value is not found, return -1

search([1,2,3,4,5,6],4) // 3
search([1,2,3,4,5,6],6) // 5
search([1,2,3,4,5,6],11) // -1

*/

/* The most basic solution but O(n) */

function search1(array, value) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] === value) {
            return i;
        }
    }
    return -1;
}

/*
    A binary search O(log(n)), applying a divide and conquer strategy
*/

function search(array, value) {
    let left = 0;
    let right = array.length - 1;
    let middle;

    while (left <= right) {
        middle = Math.floor((right + left) / 2);
        if (value === array[middle]) {
            return middle
        } else {
            if (value < array[middle]) {
                right = middle - 1;
            }
            else {
                left = middle + 1;
            }
        }
    }
    return -1;
}

let data = [
    [[1, 2, 3, 4, 5, 6], 4],
    [[1, 2, 3, 4, 5, 6], 6],
    [[1, 2, 7, 4, 5, 6], 7],
    [[1, 2, 3, 4, 5, 6], 11]
]

exports.func = search;
exports.data = data;
