/*
    Write a function called same, which accepts two arrays.
    The function should return true if every value in the array has it's corresponding value squared in the second array.
    The frequency of values must be the same.
*/

/*
    The most basic solution but O(n2)
    O(n) for the for cycle, and then inside indexOf will have another O(n)
    so same will have O(n2). Will iterate through array1, searching if its square
    exist ein the second array, if no exist return false, but if it exist, this value
    should be deleted from the second array to take frequency into account
*/

function same(array1, array2) {
    if (array1.length != array2.length) {
        return false;
    }
    for (let i = 0; i < array1.length; i++) {
        let exist = array2.indexOf(Math.pow(array1[i], 2))
        if (exist === -1) {
            return false
        }
        array2.splice(exist, 1)
    }
    return true
}

/*
    A better solution with O(n), not internal loop
    storing the frequency of the square of the values in an object so accessing is O(1).
    Will store in an object the frequency of the every element of the array1, using
    its square as key. Using a loop through array1 (O(n))

    Then loop through array2, verifying that every value of array2, really exists as a key in
    the frequency object, if not return false, if yes, should decrease the value in one,
    in that way we can check the frecuency of values is the same in both arrays

    Also, need to check all keys in the frequency object have value of 0, only in that case
    return true
*/


function same_refactored(array1, array2) {
    if (array1.length != array2.length) {
        return false;
    }
    let frequency = {}

    for (let v of array1) {
        let square = Math.pow(v, 2)
        frequency[square] === undefined ? frequency[square] = 1 : frequency[square] += 1
    }
    for (let v of array2) {
        if (frequency[v] === undefined) {
            return false
        } else {
            frequency[v] -= 1
        }
    }
    for (let f in frequency) {
        if (frequency[f] != 0) {
            return false
        }
    }
    return true;
}

/*
    Basically the same solution but with some syntax improvements,
    and using to objects to count frequency, and at the end
    compare if both object store the keys, and the square key and
    the same frequency
*/

function same_more_refactored(array1, array2) {
    if (array1.length != array2.length) {
        return false;
    }
    let frequency1 = {}
    let frequency2 = {}

    for (let v of array1) {
        frequency1[v] = (frequency1[v] || 0) + 1
    }
    for (let v of array2) {
        frequency2[v] = (frequency2[v] || 0) + 1
    }
    for (let f in frequency1) {
        let square = Math.pow(f, 2) // f**2
        if (!(square in frequency2)) { // also could be (frequency2[square] === undefined)
            return false
        }
        if (frequency1[f] !== frequency2[square]) {
            return false
        }
    }
    return true;
}

let data = [
    [[1, 2, 3, 3], [1, 9, 4, 9]],
    [[1, 5, 5, 3], [1, 25, 4, 9]]
]

exports.func = same_more_refactored;
exports.data = data;
