/*
    Write a function called linearSearchwhich accepts an array and a value,
    and returns the index at which the value exists. If the value does not exist in the array, return -1.
*/
function linearSearch(arr, v) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === v) return i;
    }
    return -1
}

let data = [
    [[10, 15, 20, 25, 30], 30]
];

exports.func = linearSearch;
exports.data = data;
