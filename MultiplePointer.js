/*
     Write a function called sumZero wich accepts a sorted array of integers.
     The function should find the first pair where the sum is 0. Return an array
     that includes both values that sum to zero or undefined if a pair does not exist

     sumZero([-3,-2,-1,0,1,2,3]) // [-3,3]
     sumZero([-2,0,1,3]) // undefined
     sumZero([1,2,3]) // undefined
*/

/* The most basic solution but O(n^2) */

function sumZero(array) {
    for (let i = 0; i < array.length; i++) {
        for (let j = i + 1; j < array.length; j++) {
            if (array[i] + array[j] === 0) {
                return [array[i], array[j]]
            }
        }
    }
}


/*
    A better solution with O(n), using to pointer one at the beginning and other to the end, will move the pointers
    while left is less than right, adding to left and subtracting to right, in case the sum was greater than 0 or not

*/

function sumZero_refactor(array) {
    let left = 0;
    let right = array.length - 1;

    while (left < right) {
        let sum = array[left] + array[right]
        if (sum === 0) {
            return [array[left], array[right]]
        } else {
            sum > 0 ? right -= 1 : left += 1;
        }
    }
}

let data = [
    [[-3, -2, -1, 0, 1, 2, 3]],
    [[-18, -2, -1, 0, 1, 2, 3, 8, 9]],
    [[-2, 0, 1, 3]],
    [[1, 2, 3]]
]

exports.func = sumZero_refactor;
exports.data = data;
