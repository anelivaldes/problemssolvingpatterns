# ProblemsSolvingPatterns

Problems Solving Patterns examples

On the root folder execute on command line:

`node index.js [Filename]

This will execute the file named Filename

Example:

`node index.js power.js`

Will output:

power(2,0) => 1

power(2,2) => 4

power(2,4) => 16

power(9,3) => 729

power(5,8) => 390625


*To add new data to test your functions, edit the file power.js
and add param sets to  data.*

To test function directly on cli
enter node console or REPL with `node`
then `.load filename.js`
