/*
    Write a function called sameFrequency.Given two positive integers,
    find out if the two numbers have the same frequency of digits.
    Your solution MUST have the following complexities:
    Time: O(N)
    sameFrequency(182, 281) // true
    sameFrequency(34, 14) // false
    sameFrequency(3589578, 5879385) // true
    sameFrequency(22, 222) // false

*/

function sameFrequency(n1, n2) {
    let s1 = n1 + '';
    let s2 = n2 + '';
    if (s1.length !== s2.length) {
        return false;
    }
    let freq = {};
    for (let i = 0; i < s1.length; i++) {
        let d = s1.charAt(i);
        freq[d] = (freq[d] || 0) + 1
    }
    for (let i = 0; i < s2.length; i++) {
        let d = s2.charAt(i);
        if (freq[d] !== undefined) {
            freq[d]--;
        } else {
            return false;
        }
    }
    for (let d in freq) {
        if (freq[d] !== 0) {
            return false;
        }
    }
    return true;
}

let data = [
    [182, 281],
    [34, 14],
    [3589578, 5879385],
    [22, 222]
]

exports.func = sameFrequency;
exports.data = data;
