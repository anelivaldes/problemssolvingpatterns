/*
 <p>Implement a function called, <strong>areThereDuplicates</strong>
  which accepts a <strong>variable number of arguments, </strong>and<em>
   </em>checks whether there are any duplicates among the arguments passed in.&nbsp;
    You can solve this using the frequency counter pattern OR the multiple pointers pattern.</p>
 */


/* With frequency pattern */
function areThereDuplicates(...args) {

    let freq = {};
    for (let a of args) {
        if (freq[a]) {
            return true;
        } else {
            freq[a] = (freq[a] || 0) + 1
        }
    }
    return false;
}

/* With multiple pointers pattern */

function areThereDuplicates2(...args) {
    //TODO: Implement this
}

let data = [
    [1, 23, 22, 28, 5, 2, 8],
    [1, 23, 22, 28, 5, 2, 2]
]

exports.func = areThereDuplicates;
exports.data = data;

