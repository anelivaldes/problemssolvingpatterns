/*
    Write a function called averagePair.Given a sorted array of integers and a target average,
    determine if there is a pair of values in the array where the average of the pair equals the
    target average. There may be more than one pair that matches the average target.

    Time: O(N)
    Space: O(1)

    averagePair([1,2,3], 2.5) true
    averagePair([1,3,3,5,6,7,10,12,19], 8) true
    averagePair([-1,0,3,4,5,6], 4,1) false
    averagePair([],4) false

*/

/*

The idea is that as the array is sorted, at the end will be the biggest values, and
at the beginning the smallest, as we only need return if a pair exist is enough
use to pointer and compare the average with the given value. To be more optimums
if we get a value greater of the average then need to sum smaller values so
we move the right or max pointer -- because on left(max started at the last item),
in the other case the sum is smaller than average we need to add more to the sum so move
the left pointer to the right ++, to get a bigger value.

*/

function averagePair(array, average) {
    let min = 0;
    let max = array.length - 1;
    let avg;
    while (min < max) {
        avg = (array[min] + array[max]) / 2;
        if (avg === average) {
            return true;
        } else {
            if (avg > average) {
                max--
            } else {
                min++
            }
        }
    }
    return false;
}

let data = [
    [[1, 2, 3], 2.5],
    [[1, 3, 3, 5, 6, 7, 10, 12, 19], 8],
    [[-1, 0, 3, 4, 5, 6], 4.1],
    [[], 4],
    [[12, 1, 8, 9], 10]

]

exports.func = averagePair;
exports.data = data;
