function binarySearch(arr, v) {
    // Assume that the array is sorted
    let min = 0;
    let max = arr.length - 1
    let middle = Math.floor((min + max) / 2)

    while (min <= max) {
        if (arr[middle] === v) {
            return middle;
        } else {
            if (v < arr[middle]) {
                max = middle - 1;
            } else {
                min = middle + 1;
            }
            middle = Math.floor((min + max) / 2);
        }
    }
    return -1
}


let data = [
    [[1, 5, 9, 25, 30], 25],
    [[1, 5, 9, 25, 30], 1],
    [[1, 5, 9, 25, 30], 9],
    [[1, 5, 9, 25, 30], 8],
    [[1, 5, 9, 25, 30], 30]
];

exports.func = binarySearch;
exports.data = data;
