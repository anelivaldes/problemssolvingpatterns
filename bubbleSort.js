function bubbleSort(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        for (let j = 0; j <= i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                let aux = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = aux;
            }
        }
    }
    return arr;
}

let data = [
    [[8,6,8,7,79,14,1,5]]
];

exports.func = bubbleSort;
exports.data = data;
