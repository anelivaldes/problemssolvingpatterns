function capitalizeFirst(array) {
    function capitalize(str) {
        return str[0].toUpperCase() + str.slice(1)
    }
    if (array.length === 1) {
        return [capitalize(array[0])];
    }
    let res = capitalizeFirst(array.slice(0, -1));
    res.push(capitalize(array[array.length - 1]));
    return res;
}

capitalizeFirst(['car', 'taco', 'banana']); // ['Car','Taco','Banana']

const capitalizeFirstV1 = (array) => {
    return array.map((value) => value[0].toUpperCase() + value.slice(1))
}

let data = [
    [['car', 'taco', 'banana']],
    [['home', 'casa', 'baby']]
];



exports.func = capitalizeFirstV1;
exports.data = data;
