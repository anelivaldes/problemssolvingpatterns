function capitalizeWords(array) {
    if (array.length === 1) {
        return [array[0].toUpperCase()]
    }
    let c = capitalizeWords(array.slice(0, -1))
    c.push(array[array.length - 1].toUpperCase())
    return c;
}

const capitalizeWordsV1 = (array) => {
    return array.map((item) => item.split('').map((i) => i.toUpperCase()).join(''))
}

let words = ['i', 'am', 'learning', 'recursion'];
let words2 = ['la', 'version', 'uno', 'no usa recursividad'];
// capitalizedWords(words); // ['I', 'AM', 'LEARNING', 'RECURSION']


let data = [
    [words],
    [words2]
];

exports.func = capitalizeWordsV1;
exports.data = data;
