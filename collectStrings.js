function collectStrings(obj) {
    let resp = []
    for (let k in obj) {
        if (typeof obj[k] === "string") {
            resp.push(obj[k])
        } else {
            if (typeof obj[k] === "object") {
                resp  = resp.concat(collectStrings(obj[k]))
            }
        }
    }
    return resp
}


const obj = {
    stuff: "foo",
    data: {
        val: {
            thing: {
                info: "bar",
                moreInfo: {
                    evenMoreInfo: {
                        weMadeIt: "baz"
                    }
                }
            }
        }
    }
}
let data = [
    [obj]
];

exports.func = collectStrings;
exports.data = data;
//console.log(collectStrings(obj)) // ["foo", "bar", "baz"])
