function fib(n) {
    if (n <= 2) {
        return 1
    } else {
        return fib(n - 1) + fib(n - 2)
    }
}

let data = [
    [4],
    [10],
    [28],
    [35]
];

exports.func = fib;
exports.data = data;
