/*
Write a function called findLongestSubstring,
which accepts a string and returns the length of the longest
substring with all distinct characters.

* */
function findLongestSubstring() {
 // TODO: Implement this
}

let data = [
    [''],
    ['rithmschool'],
    ['thisisawesome'],
    ['thecatinthehat'],
    ['bbbbbb'],
    ['longestsubstring'],
    ['thisishowwedoit']
];

exports.func = findLongestSubstring;
exports.data = data;
