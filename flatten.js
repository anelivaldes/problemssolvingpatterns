/*

Write a recursive function called flatten which accepts an array of arrays and returns a new array with all values flattened.
flatten([1, 2, 3, [4, 5] ]) // [1, 2, 3, 4, 5]
flatten([1, [2, [3, 4], [[5]]]]) // [1, 2, 3, 4, 5]
flatten([[1],[2],[3]])
flatten([[[[1], [[[2]]], [[[[[[[3]]]]]]]]]]) // [1,2,3
*/

function flatten(arr, rest) {
    let newrest = [];
    for (let i = 0; i < arr.length; i++) {
        if (Array.isArray(arr[i])) {
            newrest = newrest.concat(flatten(arr[i]))

        } else {
            newrest.push(arr[i])
        }
    }
    return newrest
}

let data = [
    [[1, 2, 3, [4, 5]]],
    [[1, [2, [3, 4, 8], [[5]]]]],
    [[[1], [2], [3]]],
    [[[[[1], [[[2]]], [[[[[[[3]]]]]]]]]]]
];

exports.func = flatten;
exports.data = data;

//flatten([1, 2, 3, [4, 5]])
