function isPalindrome1(str) {
    function reverse(str) {
        if (str.length == 1) {
            return str[0]
        } else {
            return reverse(str.slice(1)) + str[0]
        }
    }

    return str === reverse(str)
}


function isPalindrome2(str) {
    if (str.length === 1) return true;
    if (str.length === 2) return str[0] === str[1]
    if (str[0] === str.slice(-1)) return isPalindrome1(str.slice(1, -1))
    return false;
}


let data = [
    ['awesome'],
    ['foobar'],
    ['tacocat'],
    ['amanaplanacanalpanama'],
    ['amanaplanacanalpandemonium']
];

exports.func = isPalindrome2;
exports.data = data;
