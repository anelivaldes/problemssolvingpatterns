/*

    Write a function called isSubsequence which takes in two strings and checks whether
    the characters in the first string form a subsequence of the characters in the second string. In other words,
    the function should check whether the characters in the first string appear somewhere in the second string,
    without their order changing.

    isSubsequence('hello', 'helloworld') //true
    isSubsequence('sing', 'sting') //true
    isSubsequence('abc', 'abracadabra') //true
    isSubsequence('abc', 'acb') //false (order matters)

    Time: O(N + M)
    Space O(1)
*/


function isSubsequence(str1, str2) {
    let pointer1 = 0;
    let pointer2 = 0;
    let found;
    let l = {}
    while (pointer1 < str1.length) {
        let c = str1[pointer1];
        l[c] = false;
        let found = false;
        while (pointer2 < str2.length && !found) {
            if (str2[pointer2] === c) {
                found = true
                l[c] = true
            }
            pointer2++;
        }
        pointer1++;
    }
    for (let k in l) {
        if (!l[k]) {
            return false
        }
    }
    return true;
}

let data = [
    ['hello', 'helloworld'],
    ['sing', 'sting'],
    ['abc', 'abracadabra'],
    ['abc', 'acb']
];

exports.func = isSubsequence;
exports.data = data;
