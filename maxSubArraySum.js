/*
    Write a function called maxSubarraySum which accepts an array of integers and a number called n.
    The function should calculate the maximum sum of n consecutive elements in the array.

    maxSubarraySum([1,2,5,2,8,1,5],2) // 10
    maxSubarraySum([1,2,5,2,8,1,5],4) // 17
    maxSubarraySum([4,2,1,6],1) // 6
    maxSubarraySum([4,2,1,6,2],4) // 13
    maxSubarraySum([],4) // null
*/

/*
    The most basic solution, if empty return null
    to find a max use a var max with a minimal value, could be 0 or - values, but
    as there are not restrictions should be -Infinity, to consider negative values,
    and guarantee the update of the max value. Then iterate over the arr, starting 0 index(i),
    then in and internal loop calculate the sum of the n values following the value at i, a sum value set to 0 on
    each iteration, and sum accumulates values arr[i + j], since j start 0, then the arr[i] values
    will be include on every sum, this is important, once out the inner loop, make
    a simple comparison to update the max, and return max out the main loop.

    Really inefficient algorithm on big arrays
*/

function maxSubarraySum1(arr, n) {
    if (arr.length === 0) return null;
    let max = -Infinity;
    for (let i = 0; i < arr.length; i++) {
        let sum = 0;
        for (let j = 0; j < n; j++) {
            sum += arr[i + j]
        }
        if (sum > max) {
            max = sum;
        }
    }
    return max;
}

/*
    A better solution using sliding windows pattern, the idea
    is that the window will have the size of m
    and we will moving to right, on every move
    we subtract the value on left and add the value on right
    then we obtain e nex sum value to compare to max. For
    that is necessary calculate a first sum of the initial window,
    then iterate over the array updating that value and the max.

*/
function maxSubarraySum2(arr, n) {
    if (arr.length === 0 || arr.length < n) return null;
    // Find the first sum
    let basic_sum = 0;
    let max;
    for (let i = 0; i < n; i++) {
        basic_sum += arr[i];
    }
    max = basic_sum;
    for (let j = 0; j < arr.length; j++) {
        basic_sum = basic_sum - arr[j] + arr[j + n];
        if (basic_sum > max) {
            max = basic_sum;
        }
    }
    return max;
}


/*
    Some refactor, in this case the for will start on n, to be more optimal,
    so need to subtract j -n and add j. Optional use of Math.max
*/
function maxSubarraySum(arr, n) {
    if (arr.length === 0 || arr.length < n) return null;
    let basic_sum = 0;
    for (let i = 0; i < n; i++) {
        basic_sum += arr[i];
    }
    let max = basic_sum;
    for (let j = n; j < arr.length; j++) {
        basic_sum = basic_sum - arr[j - n] + arr[j];
        max = Math.max(basic_sum, max);
    }
    return max;
}

let data = [
    [[1, 2, 5, 2, 8, 1, 5], 2],
    [[1, 2, 5, 2, 8, 1, 5], 4],
    [[4, 2, 1, 6], 1],
    [[4, 2, 1, 6, 2], 4],
    [[2, 6, 9, 2, 1, 8, 5, 6, 3], 3],
    [[], 4],
    [[4, 2, 1, 6, 2], 6]
]

exports.func = maxSubarraySum;
exports.data = data;
