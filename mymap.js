function mymap(array, fn) {
    if (array.length === 1) {
        return [fn(array[0])]
    }
    let res = mymap(array.slice(0, -1), fn)
    res.push(fn(array[array.length - 1]))
    return res
}

console.log(mymap([2, 3, 4, 5], (n) => 2 * n))

function custommap(array, fn) {
    if (array.length === 1) {
        return [fn(array[0])];
    } else {
        // return [fn(array.slice(0, 1))].concat(custommap(array.slice(1), fn))
        return [fn(array.slice(0, 1)), ...custommap(array.slice(1), fn)]
    }
}

console.log(custommap([2, 3, 4, 5, 6], (n) => 4 * n))