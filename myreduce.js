const sumReducer = (accumulator, currentValue, index, array) => accumulator + currentValue;
const concatReducer = (accumulator, currentValue, index, array) => accumulator.concat(currentValue);

const numbers = [1, 2, 3, 5, 6, 7, 8, 9];
const arrays = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10]];

function myReducer(array, reducerFn, initialValue) {
  if (Array.isArray(array)) {
    if (array.length > 0) {
      let result = initialValue;
      if (typeof initialValue === 'undefined') {
        if (Array.isArray(array[0])) {
          result = [];
        } else {
          result = 0;
        }
      }
      for (let i = 0; i < array.length; i++) {
        result = reducerFn(result, array[i], i, array);
      }
      return result;
    }
    return initialValue;
  } else {
    throw new Error('The first param should be an array');
  }
}

// Sum reducer
const sumOfNumbersNative = numbers.reduce(sumReducer);
console.log('Native reduce function: ' + sumOfNumbersNative);

const sumOfNumbersMy = myReducer(numbers, sumReducer);
console.log('My reduce function: ' + sumOfNumbersMy);

const sumOfNumbersMy1 = myReducer(numbers, sumReducer, 100);
console.log('My reduce function with initial value: ' + sumOfNumbersMy1);

// Concat reducer
const concatNative = arrays.reduce(concatReducer, ['a', 'b']);
console.log('Native reduce function: ' + concatNative);

const concatMy = myReducer(arrays, concatReducer);
console.log('My reduce function: ' + concatMy);

const concatMy1 = myReducer(arrays, concatReducer, ['a', 'b']);
console.log('My reduce function with initial value: ' + concatMy1);
