function nestedEvenSum(obj) {
    let s = 0
    for (let k in obj) {
        let v = obj[k];
        if (typeof v === 'object') {
            s += nestedEvenSum(obj[k])
        } else {
            if ((typeof v === 'number') && v % 2 === 0) {
                s += v
            }
        }
    }
    return s;
}

var obj1 = {
    outer: 2,
    obj: {
        inner: 2,
        otherObj: {
            superInner: 2,
            notANumber: true,
            alsoNotANumber: "yup"
        }
    }
}

var obj2 = {
    a: 2,
    b: {b: 2, bb: {b: 3, bb: {b: 2}}},
    c: {c: {c: 2}, cc: 'ball', ccc: 5},
    d: 1,
    e: {e: {e: 2}, ee: 'car'}
};

let data = [
    [obj1],
    [obj2]
];

exports.func = nestedEvenSum;
exports.data = data;

//nestedEvenSum(obj1); // 6
//nestedEvenSum(obj2); // 10
