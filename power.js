/*

    Write a function called power which accepts a base and an exponent.
    The function should return the power of the base to the exponent.
    This function should mimic the functionality of Math.pow() - do not worry about negative bases and exponents.

    power(2,0) // 1
    power(2,2) // 4
    power(2,4) // 16

*/


function power(base, expo) {
  if (expo === 0) {
    return 1
  } else {
    return base * power(base, expo - 1)
  }
}

const powerV1 = (base, exp) => {
  return exp === 0 ? 1 : base * powerV1(base, exp - 1)
};

let data = [
  [2, 0], [2, 2], [2, 4], [9, 3], [5, 8]
];

exports.func = powerV1;
exports.data = data;
