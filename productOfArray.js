function productOfArray(array) {
    if(array.length === 0){
        return 1
    }else{
        return array[0] * productOfArray(array.slice(1));
    }
}

let data = [
    [[2, 3, 5, 4, 8]],
    [[1, 2, 3]],
    [[1, 2, 3, 10]]
];

exports.func = productOfArray;
exports.data = data;
