function recursiveRange(n) {
    if (n === 0) {
        return 0
    }
    else {
        return n + recursiveRange(n - 1);
    }
}

let data = [
    [6],
    [10]
];

exports.func = recursiveRange;
exports.data = data;
