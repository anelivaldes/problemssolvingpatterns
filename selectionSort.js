/*
Selection sort
*/
function swap(arr, i, j) {
  let t = arr[i];
  arr[i] = arr[j];
  arr[j] = t;
}

function selectionSortIterativo(arr) {
  for (let i = 0; i < arr.length; i++) {
    let min = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[min]) {
        min = j;
      }
    }
    if (i !== min) {
      swap(arr, i, min);
    }
  }
  return arr;
}
// Recursive
function selectionSort(arr) {
  if (arr.length === 0) {
    return []
  } else {
    let min = 0;
    for (let j = 1; j < arr.length; j++) {
      if (arr[j] < arr[min]) {
        min = j;
      }
    }
    if (0 !== min) {
      swap(arr, 0, min);
    }
    return [arr[0]].concat(selectionSort(arr.slice(1)));
  }
}


let data = [
  [[8, 6, 8, 2, 45, 1, 4, 89, 0, 7, 79, 14, 1, 5]]
];

// selectionSort([8, 6, 8, 2, 4]);
exports.func = selectionSort;
exports.data = data;
