function isOdd(v) {
    return v % 2 !== 0;
}

function someRecursive(arr, callback) {
    if (arr.length === 0) {
        return false;
    } else {
        if (callback(arr[0]) === true) {
            return true;
        }
        else {
            return someRecursive(arr.slice(1), callback)
        }
    }
}

let data = [
    [[1, 2, 8, 5], isOdd],
    [[4, 6, 8, 9], isOdd],
    [[4, 6, 8], isOdd],
    [[4, 6, 8], val => val > 17]
]

exports.func = someRecursive;
exports.data = data;
