function test(f, data) {
    function format(data) {
        let params = []
        for (let d of data) {
            let p
            if (Array.isArray(d)) {
                p = '[' + d.join(',') + ']'
            }
            else {
                if (typeof d === "string") {
                    p = "'" + d + "'"

                } else {
                    if (typeof d === "function") {
                        p = d.name
                    } else {
                        p = d.toString();
                    }


                }
            }
            params.push(p)
        }
        return params.join(',')
    }

    for (let d of data) {
        console.log(f.name + '(' + format(d) + ') => ' + f(...d));
    }
}

module.exports = test;
